#!/bin/sh
#
# This file is part of the LibreOffice project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

# simple wrapper script to run non-installed executables from workdir

if [ $(uname) = Darwin ]; then

dir=$(pwd)

if [ ! -d "${dir}/instdir/LibreOffice.app" ]; then
    echo "error: cannot find \"instdir/LibreOffice.app\" dir in \"$(pwd)\""
    exit 1
fi

exedir="${dir}"/workdir/LinkTarget/Executable
export URE_BOOTSTRAP=file://"${dir}"/instdir/LibreOffice.app/Contents/Resources/fundamentalrc
export DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH:+$DYLD_LIBRARY_PATH:}"${dir}"/instdir/LibreOffice.app/Contents/Frameworks

echo "setting URE_BOOTSTRAP to: ${URE_BOOTSTRAP}"
echo "setting search path to: ${DYLD_LIBRARY_PATH}"
echo "execing: ${exedir}/$1"

exec "${exedir}"/$@

else

dir=$(realpath "$(pwd)")

while test ! -d "${dir}/instdir/program" ; do
    if test "${dir}" = "/"; then
        echo "error: cannot find \"program\" dir from \"$(pwd)\""
        exit 1
    fi
    dir=$(realpath "${dir}/..")
done

exedir="${dir}"/workdir/LinkTarget/Executable
export URE_BOOTSTRAP=file://"${dir}"/instdir/program/fundamentalrc
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}"${dir}"/instdir/ure/lib:"${dir}"/instdir/program

echo "setting URE_BOOTSTRAP to: ${URE_BOOTSTRAP}"
echo "setting search path to: ${LD_LIBRARY_PATH}"
echo "execing: ${exedir}/$1"

cd "${dir}"/instdir/program
exec "${exedir}"/$@

fi
